import { PLATFORM } from "aurelia-framework";

import {RouterConfiguration, Router, NavigationInstruction} from 'aurelia-router';

export class App {
    router: Router;
  
    configureRouter(config: RouterConfiguration, router: Router): void {
      this.router = router;
      config.title = 'Aurelia';
      const navStrat = (instruction: NavigationInstruction) => {
        instruction.config.moduleId = instruction.fragment
        instruction.config.href = instruction.fragment
      };
      
      config.map([
        { route: ['', 'Person'],  name: 'Person',  moduleId: PLATFORM.moduleName('Person/Person') },
        { route: 'Response',         name: 'Response', moduleId: PLATFORM.moduleName('Response/Response')}
      ]);
    }
}

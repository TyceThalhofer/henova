import { inject, bindable } from 'aurelia-framework';
import { ValidationRules, ValidationController,ValidationControllerFactory,ValidateEvent } from 'aurelia-validation';
import {HttpClient, HttpResponseMessage} from 'aurelia-http-client';
import {BootstrapFormRenderer} from '../FormRenderer/BootstrapFormRenderer';
import {TdlRule, CountryRule,IntegerRangeRule} from '../Validation/CustomRules';
import { String } from 'typescript-string-operations';
import { Router } from 'aurelia-router';
import { Data } from './Data'
import * as config from '../../config/config.json';
import * as environment from '../../config/environment.json';


@inject(ValidationControllerFactory,Router, HttpClient)
export class Person  {
  @bindable Name: string;
  @bindable FamilyName: string;
  @bindable Address: string;
  @bindable CountryOfOrigin: string;
  @bindable Email: string;
  @bindable Age: number;
  @bindable Controller: ValidationController; 
  @bindable IsEmpty: boolean;
  @bindable IsValid: boolean;

  Router: Router;
  RestCient: HttpClient;

  BaseUrl: string;
  Api: string;

  constructor(ValidationControllerFactory: ValidationControllerFactory, Router: Router, RestCient : HttpClient) {
  
    this.BaseUrl = environment.debug ?  config.Debug.BaseUrl : config.BaseUrl;
    this.Api = environment.debug ?  config.Debug.Api : config.Api;

    this.ApplyValidationRules();

    this.Controller = ValidationControllerFactory.createForCurrentScope();
    this.Controller.subscribe((event: ValidateEvent)   =>  {
       this.IsValid =  this.Controller.errors.length == 0;
       this.IsEmpty = String.IsNullOrWhiteSpace(this.Name);
    });
    this.Controller.addRenderer(new BootstrapFormRenderer());

    this.Router = Router;
    this.RestCient = RestCient
    .configure(x => {
      x.withBaseUrl(this.BaseUrl);
      x.withHeader('Content-Type', 'application/json');
    });
      

    this.IsValid = false;
    this.IsEmpty = true;
  }

  ApplyValidationRules() {
    let TdlRuleName =  TdlRule();
    let CountryRuleName = CountryRule(); 
    let IntegerRangeRuleName = IntegerRangeRule();
    ValidationRules
      .ensure((Person: Person) => Person.Name)
      .required()
      .minLength(5)
      .withMessage("must be at least 5 character long")
      .ensure((Person: Person) => Person.FamilyName)
      .required()
      .minLength(5)
      .withMessage("must be at least 5 character long")
      .ensure((Person: Person) => Person.Address)
      .required()
      .minLength(10)
      .withMessage("must be at least 10 character long")
      .ensure((Person: Person) => Person.Age)
      .required()
      .then()
      .satisfiesRule(IntegerRangeRuleName, 20, 60)
      .ensure((Person: Person) => Person.CountryOfOrigin)
      .required()
      .then()
      .satisfiesRule(CountryRuleName)
      .ensure((Person: Person) => Person.Email)
      .required()
      .then()
      .email()
      .withMessage("invalid email form (*@*.[Tdl])")
      .then()
      .satisfiesRule(TdlRuleName)
      .on(this);
  }

onReset() {
  let Form = <HTMLFormElement>document.getElementsByTagName('form').item(0);
  Form.reset();
  this.Controller.reset();
  this.IsEmpty = true;
}

 async onAdd() {
    let Result = await this.Controller.validate();
    if(!Result.valid)
    {
      return;
    } 

    let data = new Data(this.Name,this.FamilyName,this.Address,this.CountryOfOrigin,this.Email,parseInt(this.Age.toString()));
  
    let Response = <HttpResponseMessage>await this.RestCient.post(this.Api,data).catch((error) => {return error});
    let Messages = !Response.isSuccess ? (
      Response.statusCode == 400 ?
        JSON.parse(Response.response) :
      [  Response.response  ]
    ) : [];
    this.Router.navigateToRoute('Response',
    {
      IsError:  !Response.isSuccess,
      Messages: Messages,
      NavigateTo: "Person"
    });    
  }
}


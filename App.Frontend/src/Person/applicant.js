var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { inject, bindable } from 'aurelia-framework';
import { ValidationRules, ValidationController, ValidationControllerFactory } from 'aurelia-validation';
import { HttpClient } from 'aurelia-http-client';
import { BootstrapFormRenderer } from '../FormRenderer/BootstrapFormRenderer';
import { TdlRule, CountryRule, IntegerRangeRule } from '../Validation/CustomRules';
import { String } from 'typescript-string-operations';
import { Router } from 'aurelia-router';
var Data = (function () {
    function Data(Name, FamilyName, Address, CountryOfOrigin, Email, Age, Hired) {
        this.Name = Name;
        this.FamilyName = FamilyName;
        this.Address = Address;
        this.CountryOfOrigin = CountryOfOrigin;
        this.EmailAddress = Email;
        this.Age = Age;
        this.Hired = Hired;
    }
    return Data;
}());
var applicant = (function () {
    function applicant(ValidationControllerFactory, Router, RestCient) {
        var _this = this;
        this.ApplyValidationRules();
        this.Controller = ValidationControllerFactory.createForCurrentScope();
        this.Controller.subscribe(function (event) {
            _this.IsValid = _this.Controller.errors.length == 0;
            _this.IsEmpty = String.IsNullOrWhiteSpace(_this.Name);
        });
        this.Controller.addRenderer(new BootstrapFormRenderer());
        this.Router = Router;
        this.RestCient = RestCient;
        this.IsValid = false;
        this.IsEmpty = true;
    }
    applicant.prototype.ApplyValidationRules = function () {
        var TdlRuleName = TdlRule();
        var CountryRuleName = CountryRule();
        var IntegerRangeRuleName = IntegerRangeRule();
        ValidationRules
            .ensure(function (applicant) { return applicant.Name; })
            .required()
            .minLength(5)
            .withMessage("must be at least 5 character long")
            .ensure(function (applicant) { return applicant.FamilyName; })
            .required()
            .minLength(5)
            .withMessage("must be at least 5 character long")
            .ensure(function (applicant) { return applicant.Address; })
            .required()
            .minLength(10)
            .withMessage("must be at least 10 character long")
            .ensure(function (applicant) { return applicant.Age; })
            .required()
            .then()
            .satisfiesRule(IntegerRangeRuleName, 20, 60)
            .ensure(function (applicant) { return applicant.CountryOfOrigin; })
            .required()
            .then()
            .satisfiesRule(CountryRuleName)
            .ensure(function (applicant) { return applicant.Email; })
            .required()
            .then()
            .email()
            .withMessage("invalid email form (*@*.[Tdl])")
            .then()
            .satisfiesRule(TdlRuleName)
            .on(this);
    };
    applicant.prototype.onReset = function () {
        var Form = document.getElementsByTagName('form').item(0);
        Form.reset();
        this.Controller.reset();
        this.IsEmpty = true;
    };
    applicant.prototype.onAdd = function () {
        return __awaiter(this, void 0, void 0, function () {
            var Result, data, client, Response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4, this.Controller.validate()];
                    case 1:
                        Result = _a.sent();
                        if (!Result.valid) {
                            return [2];
                        }
                        data = new Data(this.Name, this.FamilyName, this.Address, this.CountryOfOrigin, this.Email, parseInt(this.Age.toString()), this.Hired);
                        client = new HttpClient()
                            .configure(function (x) {
                            x.withBaseUrl("https://localhost:5001/");
                            x.withHeader('Content-Type', 'application/json');
                        });
                        return [4, client.post("Applicant/", data).catch(function (error) { return error; })];
                    case 2:
                        Response = _a.sent();
                        console.log(Response);
                        this.Router.navigateToRoute('Response', {
                            IsError: !Response.isSuccess,
                            Messages: !Response.isSuccess ?
                                (Response.statusCode == 400 ?
                                    JSON.parse(Response.response) :
                                    [
                                        Response.response
                                    ]) : []
                        });
                        return [2];
                }
            });
        });
    };
    __decorate([
        bindable,
        __metadata("design:type", String)
    ], applicant.prototype, "Name", void 0);
    __decorate([
        bindable,
        __metadata("design:type", String)
    ], applicant.prototype, "FamilyName", void 0);
    __decorate([
        bindable,
        __metadata("design:type", String)
    ], applicant.prototype, "Address", void 0);
    __decorate([
        bindable,
        __metadata("design:type", String)
    ], applicant.prototype, "CountryOfOrigin", void 0);
    __decorate([
        bindable,
        __metadata("design:type", String)
    ], applicant.prototype, "Email", void 0);
    __decorate([
        bindable,
        __metadata("design:type", Number)
    ], applicant.prototype, "Age", void 0);
    __decorate([
        bindable,
        __metadata("design:type", Boolean)
    ], applicant.prototype, "Hired", void 0);
    __decorate([
        bindable,
        __metadata("design:type", ValidationController)
    ], applicant.prototype, "Controller", void 0);
    __decorate([
        bindable,
        __metadata("design:type", Boolean)
    ], applicant.prototype, "IsEmpty", void 0);
    __decorate([
        bindable,
        __metadata("design:type", Boolean)
    ], applicant.prototype, "IsValid", void 0);
    applicant = __decorate([
        inject(ValidationControllerFactory, Router, HttpClient),
        __metadata("design:paramtypes", [ValidationControllerFactory, Router, HttpClient])
    ], applicant);
    return applicant;
}());
export { applicant };
//# sourceMappingURL=applicant.js.map
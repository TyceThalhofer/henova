import { IPerson } from './IPerson'

export class Data implements IPerson
{
  Id : any;
  Name: string;
 FamilyName: string;
  Address: string;
  CountryOfOrigin: string;
  EmailAddress: string;
  Age: number;
  IsEmpty: boolean;
  IsValid: boolean;
  constructor(Name: string, FamilyName: string, Address: string, CountryOfOrigin: string, Email: string, Age: number)
  {
    this.Name = Name;
    this.FamilyName = FamilyName;
    this.Address = Address;
    this.CountryOfOrigin = CountryOfOrigin;
    this.EmailAddress = Email;
    this.Age = Age;
  }
}
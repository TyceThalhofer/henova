import {RenderInstruction, ValidateResult } from 'aurelia-validation';
  

// copied from https://gist.github.com/jdanyow/381fdb1a4b0865a4c25026187db865ce
  export class BootstrapFormRenderer {
    render(instruction: RenderInstruction) {
      for (let { result, elements } of instruction.unrender) {
        for (let element of elements) {
          this.remove(element, result);
        }
      }
  
      for (let { result, elements } of instruction.render) {
        for (let element of elements) {
          this.add(element, result);
        }
      }
    }
  
    add(element: Element, result: ValidateResult) {
      if (result.valid) {
        return;
      }

      const formGroup = element.closest('.form-group');
      if (!formGroup) {
        return;
      }
  
      formGroup.classList.add('has-error');
      const message = document.createElement('span');
      message.className = 'help-block validation-message';
      message.textContent = result.message;
      formGroup.appendChild(message);
    }
  
    remove(element: Element, result: ValidateResult) {
      if (result.valid) {
        return;
      }
      const formGroup = element.closest('.form-group');
      if (!formGroup) {
        return;
      }
  
      const message = formGroup.querySelector(`.validation-message`);
      if (message) {
        formGroup.removeChild(message);
        {
          formGroup.classList.remove('has-error');
        } 
      }
    }
  }
  
var BootstrapFormRenderer = (function () {
    function BootstrapFormRenderer() {
    }
    BootstrapFormRenderer.prototype.render = function (instruction) {
        for (var _i = 0, _a = instruction.unrender; _i < _a.length; _i++) {
            var _b = _a[_i], result = _b.result, elements = _b.elements;
            for (var _c = 0, elements_1 = elements; _c < elements_1.length; _c++) {
                var element = elements_1[_c];
                this.remove(element, result);
            }
        }
        for (var _d = 0, _e = instruction.render; _d < _e.length; _d++) {
            var _f = _e[_d], result = _f.result, elements = _f.elements;
            for (var _g = 0, elements_2 = elements; _g < elements_2.length; _g++) {
                var element = elements_2[_g];
                this.add(element, result);
            }
        }
    };
    BootstrapFormRenderer.prototype.add = function (element, result) {
        if (result.valid) {
            return;
        }
        var formGroup = element.closest('.form-group');
        if (!formGroup) {
            return;
        }
        formGroup.classList.add('has-error');
        var message = document.createElement('span');
        message.className = 'help-block validation-message';
        message.textContent = result.message;
        formGroup.appendChild(message);
    };
    BootstrapFormRenderer.prototype.remove = function (element, result) {
        if (result.valid) {
            return;
        }
        var formGroup = element.closest('.form-group');
        if (!formGroup) {
            return;
        }
        var message = formGroup.querySelector(".validation-message");
        if (message) {
            formGroup.removeChild(message);
            {
                formGroup.classList.remove('has-error');
            }
        }
    };
    return BootstrapFormRenderer;
}());
export { BootstrapFormRenderer };
//# sourceMappingURL=BootstrapFormRenderer.js.map
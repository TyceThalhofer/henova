var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { bindable, inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
var Response = (function () {
    function Response(Router) {
        this.Router = Router;
    }
    Response.prototype.activate = function (params) {
        var _this = this;
        this.IsError = params.IsError == "true" ? true : false;
        if (this.IsError == true) {
            this.Heading = "Error";
        }
        else {
            this.Heading = "Success";
        }
        this.Messages = params.Messages;
        setTimeout(function () { return _this.Router.navigate('Applicant'); }, this.IsError ? 10 * 1000 : 5 * 1000);
    };
    __decorate([
        bindable,
        __metadata("design:type", Boolean)
    ], Response.prototype, "IsError", void 0);
    __decorate([
        bindable,
        __metadata("design:type", Array)
    ], Response.prototype, "Messages", void 0);
    __decorate([
        bindable,
        __metadata("design:type", String)
    ], Response.prototype, "Heading", void 0);
    Response = __decorate([
        inject(Router),
        __metadata("design:paramtypes", [Router])
    ], Response);
    return Response;
}());
export { Response };
//# sourceMappingURL=Response.js.map
import { bindable, bindingMode, inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

@inject(Router)

export class Response {

    @bindable IsError: boolean;
    @bindable Messages: string[];
    @bindable Heading : string;

    Router: Router;
    constructor(Router: Router) {
        this.Router = Router;
    }
    activate(params) {
        this.IsError = params.IsError == "true" ? true : false;
        if(this.IsError == true)
        {
            this.Heading = "Error";
        }
        else
        {
            this.Heading = "Success";
        }
        this.Messages = params.Messages;
        if(params.NavigateTo != null)
        {
            setTimeout(() => this.Router.navigate(params.NavigateTo), this.IsError ? 10 * 1000  : 5 * 1000);
        }
    }
}
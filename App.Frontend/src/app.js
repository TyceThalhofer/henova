import { PLATFORM } from "aurelia-framework";
var App = (function () {
    function App() {
    }
    App.prototype.configureRouter = function (config, router) {
        this.router = router;
        config.title = 'Aurelia';
        var navStrat = function (instruction) {
            instruction.config.moduleId = instruction.fragment;
            instruction.config.href = instruction.fragment;
        };
        config.map([
            { route: ['', 'Applicant'], name: 'Applicant', moduleId: PLATFORM.moduleName('Applicant/Applicant') },
            { route: 'Response', name: 'Response', moduleId: PLATFORM.moduleName('Response/Response') }
        ]);
    };
    return App;
}());
export { App };
//# sourceMappingURL=app.js.map
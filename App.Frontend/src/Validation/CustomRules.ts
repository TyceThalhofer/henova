import {IValidationClient, ValidateViaApiClient, ValidateViaRessourceClient} from './ValidationClient';
import { ValidationRules } from 'aurelia-validation';

const CountryValidationApi: string = "https://restcountries.eu/rest/v2/name/{0}?fullText=true"
export function CountryRule(): string
 {
    return CustomClientRule(
    'ValidateCountry',
    new ValidateViaApiClient(CountryValidationApi),
    "Unkown Country");
}


const TdlRecourseUrl = "https://data.iana.org/TLD/tlds-alpha-by-domain.txt";
const TdlNeedle = "\n{0:U}\n";
function ExtractTdl(Url: string) : string
{
   return Url.split('.').pop();
}

export function TdlRule() : string
{
    return CustomClientRule(
        'InvalidTDL', 
        new ValidateViaRessourceClient(TdlRecourseUrl,TdlNeedle, false,ExtractTdl),  
        "invalid TDL.");
}

export function CustomClientRule(Name: string, Client : IValidationClient, Message: string) : string
{
  ValidationRules.customRule(
    Name,
     (value, obj) => {
       return  Client.validate(value);
    },
    Message
  );
  return Name;
}

const IntergerRuleName : string = 'IntegerRange';
export function IntegerRangeRule() : string
{
// copied from https://gist.github.com/y2k4life/8adacec7627d792a304f62a899f21049
     ValidationRules.customRule(
        IntergerRuleName,
        (value, obj, min, max) => {
            return value === null || value === undefined
            ||   value >= min && value <= max},
        'must be an integer between \${$config.min} and \${$config.max}.',
    (min, max) => ({ min, max }));
    return IntergerRuleName;
}
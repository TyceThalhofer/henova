import { ValidateViaApiClient, ValidateViaRessourceClient } from './ValidationClient';
import { ValidationRules } from 'aurelia-validation';
var CountryValidationApi = "https://restcountries.eu/rest/v2/name/{0}?fullText=true";
export function CountryRule() {
    return CustomClientRule('ValidateCountry', new ValidateViaApiClient(CountryValidationApi), "Unkown Country");
}
var TdlRecourseUrl = "https://data.iana.org/TLD/tlds-alpha-by-domain.txt";
var TdlNeedle = "\n{0:U}\n";
function ExtractTdl(Url) {
    return Url.split('.').pop();
}
export function TdlRule() {
    return CustomClientRule('InvalidTDL', new ValidateViaRessourceClient(TdlRecourseUrl, TdlNeedle, false, ExtractTdl), "invalid TDL.");
}
export function CustomClientRule(Name, Client, Message) {
    ValidationRules.customRule(Name, function (value, obj) {
        return Client.validate(value);
    }, Message);
    return Name;
}
var IntergerRuleName = 'IntegerRange';
export function IntegerRangeRule() {
    ValidationRules.customRule(IntergerRuleName, function (value, obj, min, max) {
        return value === null || value === undefined
            || value >= min && value <= max;
    }, 'must be an integer between \${$config.min} and \${$config.max}.', function (min, max) { return ({ min: min, max: max }); });
    return IntergerRuleName;
}
//# sourceMappingURL=CustomRules.js.map
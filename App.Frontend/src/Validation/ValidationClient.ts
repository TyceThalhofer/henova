
import { ValidationRules} from 'aurelia-validation';
import { HttpClient } from 'aurelia-http-client';
import { String } from 'typescript-string-operations';


export interface IValidationClient
{
  validate(Value: string) : Promise<boolean>
}

export class ValidateViaApiClient extends HttpClient implements IValidationClient
{
    Api : string;
    NullIsValidValue : boolean;
    constructor(Api: string, NullIsValidValue: boolean = false)
    {
        super();
        this.Api = Api
        this.NullIsValidValue = NullIsValidValue
    }
    
    async validate(Value : string) : Promise<boolean>
    {
        if(String.IsNullOrWhiteSpace(Value)) {
            return  this.NullIsValidValue;
        }         
        
         let Result = await this.get(String.Format(this.Api,Value))
            .catch(() =>{return null});
        return Result !=null;
    }
}






export class ValidateViaRessourceClient extends HttpClient implements IValidationClient
{
  Url: string;
  Needle: string;
  NullIsValidValue: boolean;
  PreValueManipulation: (input: string) => string

  constructor(Url: string, Needle: string, NulllIsVaidValue: boolean = false,PreValueManipulation: (input: string) => string = (Input: string) =>{return Input})
  {
    super();
    this.Url = Url;
    this.Needle = Needle;
    this.NullIsValidValue = NulllIsVaidValue;
    this.PreValueManipulation = PreValueManipulation;
  }
  async validate(Value: string) : Promise<boolean>
  {
    if(String.IsNullOrWhiteSpace(Value))
    {
      return this.NullIsValidValue;
    }
      let Response = await this.get(String.Format(this.Url))
      .catch(() => {return null});
    if(Response == null)
    {
      return false;
    }
    let ValueForNeedle = this.PreValueManipulation(Value);
    return Response.response.includes(String.Format(this.Needle,ValueForNeedle));
  }
}
﻿using System;

namespace App.Data
{

    public class EntityNotFoundException
        : Exception
    {
        public EntityNotFoundException(int Id)
            : base(string.Format("Requested entity.ID=={0} not found", Id))
        {
        }
    }
}
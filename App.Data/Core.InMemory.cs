﻿using System;

namespace App.Data
{
    namespace Core
    {
        public partial class InMemory<T>
            : IDataBase<T>
            where
                T : class, IEntity
        {
            public InMemory()
            {
            }
            public int Add(T Entity)
            {
                if (Entity == null)
                {
                    throw new ArgumentNullException(nameof(Entity));
                }
                if (Entity.ID.HasValue)
                {
                    throw new BadUsageException(
                            string.Format
                            (
                                "The value Entity.ID=={0} should be null for calling IDataBase<T>.Add(T Entity). Use IDataBase<T>.Update(T Entity) to modify existing entities",
                                Entity.ID.Value
                            )
                        );
                }
                DbContext Context = new DbContext();
                Context.Entities.Add(Entity);
                Context.SaveChanges();
                return Entity.ID.Value;
            }
            public T Get(int ID)
            {
                DbContext Context = new DbContext();
                return Context.Entities.Find(ID) ?? throw new EntityNotFoundException(ID);
            }
            public void Update(T Entity)
            {
                if (Entity == null)
                {
                    throw new ArgumentNullException(nameof(Entity));
                }
                if (!Entity.ID.HasValue)
                {
                    throw new BadUsageException
                            (
                                "The value Entity.ID==null should be an integer for calling IDataBase<T>.Update(T Entity). Use IDataBase<T>.Add(T Entity) to add new entities"
                            );
                }
                DbContext Context = new DbContext();
                Context.Entities.Update(Entity);
                Context.SaveChanges();
            }
            public void Remove(T Entity)
            {
                if (Entity == null)
                {
                    throw new ArgumentNullException(nameof(Entity));
                }
                DbContext Context = new DbContext();
                Context.Entities.Remove(Entity);
                Context.SaveChanges();
            }
        }
    }
}
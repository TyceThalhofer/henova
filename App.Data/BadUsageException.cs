﻿using System;

namespace App.Data
{
    public class BadUsageException
        : Exception
    {
        public BadUsageException(string Reason)
            : base(string.Format("Bad Usage of Db: {0}.", Reason))
        {
        }
    }
}
﻿using App.Data.Core;

namespace App.Data
{
    namespace Person
    {
        public class DataBase<T>
            : IDataBase<T>
            where
                T : Entity
        {
            private readonly IDataBase<T> BaseDb = new InMemory<T>();
            public DataBase()
            {
            }
            public int Add(T Entry)
            {
                return BaseDb.Add(Entry);
            }

            public T Get(int ID)
            {
                return BaseDb.Get(ID);
            }

            public void Remove(T Entity)
            {
                BaseDb.Remove(Entity);
            }

            public void Update(T Entity)
            {
                BaseDb.Update(Entity);
            }
        }
    }
}

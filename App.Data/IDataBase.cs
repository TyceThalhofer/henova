﻿
namespace App.Data
{
    public interface IDataBase<T>
        where
            T : class, IEntity
    {
        int Add(T Entry);
        T Get(int ID);
        void Update(T Entity);
        void Remove(T Entity);
    }
}
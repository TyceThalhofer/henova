﻿using Microsoft.EntityFrameworkCore;

namespace App.Data
{
    namespace Core
    {
        public partial class InMemory<T>
                  : IDataBase<T>
            where
                T : class, IEntity
        {
            private class DbContext : Microsoft.EntityFrameworkCore.DbContext
            {

                private static readonly DbContextOptions<DbContext> Options = new DbContextOptionsBuilder<DbContext>()
                    .UseInMemoryDatabase(databaseName: C.DataBaseName)
                    .Options;
                internal DbContext()
                    : base(Options)
                {
                }
                internal DbSet<T> Entities
                {
                    get;
                    set;
                }
            }
        }
    }
}

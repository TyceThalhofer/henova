﻿namespace App.Data
{
    namespace Person
    {
        public class Entity
            : Core.Entity
        {
            public virtual string Name
            {
                get;
                set;
            }
            public virtual string FamilyName
            {
                get;
                set;
            }
            public virtual string Address
            {
                get;
                set;
            }
            public virtual string CountryOfOrigin
            {
                get;
                set;
            }
            public virtual string EMailAddress
            {
                get;
                set;
            }
            public virtual int? Age
            {
                get;
                set;
            }
        }
    }
}

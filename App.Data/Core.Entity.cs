﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Data
{
    namespace Core
    {
        public abstract class Entity : IEntity
        {
            [Key]
            [Column(Order = 1)]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int? ID
            {
                get;
                private set;
            }
        }
    }
}

﻿namespace App.Domain
{
    namespace Person
    {
        public partial class Validator
             : Core.Validation.Validator<Element>
        {
            public static class C
            {
                public const int MIN_NAME_LENGTH = 5;
                public const int MIN_FAMILYNAME_LENGTH = 5;
                public const int MIN_ADDRESS_LENGTH = 10;
                public const int MIN_AGE = 20;
                public const int MAX_AGE = 60;
                internal const string REST_API_COUNTRIES = "https://restcountries.eu/rest/v2/name/{0}?fullText=true";
                internal const string LINK_TO_LIST_OF_ALL_VALID_TDL = "https://data.iana.org/TLD/tlds-alpha-by-domain.txt";
                internal const string SEARCH_NEEDLE_TLD_LIST = "\n{0}\n";
            }
        }
    }
}

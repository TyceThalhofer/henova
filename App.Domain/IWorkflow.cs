﻿namespace App.Domain
{
    public interface IWorkflow<T>
        where
            T : IElement
    {
        void Add(T Element);
        void Remove(T Element);
        void Update(T Element);
        T Get(int ID);
    }
}

﻿

namespace App.Domain
{
    namespace Core.Validation
    {
        public interface IValidator<T>
            where
                T : class, IElement
        {
            void Validate(T Element);
        }
    }
}


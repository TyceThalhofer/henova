﻿using App.Data;

namespace App.Domain
{
    namespace Core.Validation
    {
        public abstract class Workflow<U, V, W>
            : IWorkflow<U>
            where
                U : class, IElement
            where
                V : IValidator<U>, new()
            where
                W : IDataBase<U>, new()
        {
            private IDataBase<U> DataBase
            {
                get;
                set;
            }
            protected Workflow()
            {
                DataBase = new W();
            }
            private void Validate(U Element)
            {
                IValidator<U> Validator = new V();
                Validator.Validate(Element);
            }
            public void Add(U Element)
            {
                Validate(Element);
                DataBase.Add(Element);
            }
            public void Remove(U Element)
            {
                Validate(Element);
                DataBase.Remove(Element);
            }
            public void Update(U Element)
            {
                Validate(Element);
                DataBase.Update(Element);
            }
            public U Get(int ID)
            {
                return DataBase.Get(ID);
            }
        }
    }
}


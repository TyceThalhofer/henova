﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;

namespace App.Domain
{
    namespace Core.Validation
    {
        public abstract class Validator<T>
            : AbstractValidator<T>,
              IValidator<T>
            where
                T : class, IElement
        {
            void IValidator<T>.Validate(T Element)
            {
                ValidationResult Result = Validate(Element);
                if (!Result.IsValid)
                {
                    List<string> Messages = Result
                        .Errors
                        .ToList()
                        .ConvertAll
                        (
                            (ValidationFailure ValidationFailure) =>
                            {
                                return ValidationFailure.ErrorMessage;
                            }
                        );
                    throw new ValidationException(Messages);
                }
            }
        }
    }
}


﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using System.Linq.Expressions;
using App.Domain;


namespace App.Domain
{
    namespace Person
    {
        public partial class Validator
            : Core.Validation.Validator<Element>
        {
            #region RuleSet
            internal void MinStringLengthRule
                (
                    Expression<Func<Element, string>> Expression,
                    int MinimumLength,
                    string Message
                )
            {
                RuleFor(Expression)
                    .MinimumLength(MinimumLength)
                    .WithMessage(Message);
            }
            internal void NameValidationRule()
            {
                MinStringLengthRule
                    (
                        Appicant => Appicant.Name,
                        C.MIN_NAME_LENGTH,
                        string.Format
                            (
                                "Validation failed due to  field name: field must be at least {0} characters",
                                C.MIN_NAME_LENGTH
                            )
                    );
            }
            private void FamilyNameValidationRule()
            {
                MinStringLengthRule
                (
                    Appicant => Appicant.FamilyName,
                    C.MIN_FAMILYNAME_LENGTH,
                    string.Format
                            (
                                "Validation failed due to field FamilyName: field must be at least {0} characters",
                                C.MIN_FAMILYNAME_LENGTH
                            )
                );
            }
            private void AddressValidationRule()
            {
                MinStringLengthRule
               (
                   Appicant => Appicant.Address,
                   C.MIN_ADDRESS_LENGTH,
                   string.Format
                           (
                               "Validation failed due to field FamilyName: field must be at least {0} characters",
                               C.MIN_FAMILYNAME_LENGTH
                           )
               );
            }
            private void CountryOfOriginValidationRule()
            {
                RuleFor(Appicant => Appicant.CountryOfOrigin)
                    .MustAsync(IsValidCountry)
                    .WithMessage
                    (
                        "Validation failed due to field CountryOfOrigin: field must be a valid country"
                    );
            }
            private void EmailValidationRule()
            {
                RuleFor(Appicant => Appicant.EMailAddress)
                    .EmailAddress()
                    .MustAsync
                    (
                        (string Email, CancellationToken Token) =>
                        {
                            string TLD = ExcractTLD(Email);
                            return IsValidTLD(TLD, Token);
                        }
                     )
                    .WithMessage
                    (
                        "Validation failed due to field EMailAdress: field must be of the form *@*.[tld]"
                    );
            }
            private void AgeValidationRule()
            {
                RuleFor(Appicant => Appicant.Age)
                   .InclusiveBetween(C.MIN_AGE, C.MAX_AGE)
                   .WithMessage
                   (
                       string.Format
                           (
                               "Validation failed due to field Age: field must be a number between {0} and {1}",
                               C.MIN_AGE, C.MAX_AGE
                           )
                   );
            }
            #endregion
            public Validator()
            {
                NameValidationRule();
                FamilyNameValidationRule();
                AddressValidationRule();
                CountryOfOriginValidationRule();
                EmailValidationRule();
                AgeValidationRule();
            }
            private string ExcractTLD(string Email)
            {
                if (string.IsNullOrEmpty(Email))
                    return string.Empty;
                int StartOfTLD = Email.LastIndexOf('.');
                return Email.Substring(StartOfTLD + 1);
            }
            private async Task<bool> IsValidTLD(string TLD, CancellationToken Token)
            {
                using HttpClient Client = new HttpClient();
                using HttpResponseMessage Result = await Client.GetAsync
                    (
                        C.LINK_TO_LIST_OF_ALL_VALID_TDL,
                        Token
                    );
                if (Result.StatusCode != HttpStatusCode.OK)
                {
                    return false;
                }
                string ListOfAllValidTLD = await Result.Content.ReadAsStringAsync();
                return ListOfAllValidTLD.Contains
                    (
                        string.Format(C.SEARCH_NEEDLE_TLD_LIST, TLD),
                        StringComparison.OrdinalIgnoreCase
                    );
            }
            private async Task<bool> IsValidCountry(string Country, CancellationToken Token)
            {
                using HttpClient Client = new HttpClient();
                using HttpResponseMessage Result = await Client.GetAsync
                    (
                        string.Format(C.REST_API_COUNTRIES, Country),
                        Token
                    );
                return Result.StatusCode == HttpStatusCode.OK;
            }
        }
    }
}


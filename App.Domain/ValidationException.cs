﻿using System;
using System.Collections.Generic;

namespace App.Domain
{
    public class ValidationException
        : Exception
    {
        public IList<string> Messages
        {
            get;
        }
        public ValidationException(IList<string> Messages)
            : base()
        {
            this.Messages = Messages;
        }
    }
}

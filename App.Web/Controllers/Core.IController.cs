﻿using App.Domain;
using Microsoft.AspNetCore.Mvc;

namespace App.Web
{
    public interface IController<T>
        where T : IElement
    {
        T Get(int ID);
        public PostResponse<T> Post([FromBody] T Element);
        public void Put([FromBody] T Element);
        public void Delete([FromBody] T Element);
    }
}

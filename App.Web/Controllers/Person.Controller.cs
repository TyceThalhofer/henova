﻿using App.Domain.Person;
using App.Web.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace App.Web
{
    namespace Person
    {
        [Route(Route)]
        public class Controller : Controller<Workflow, Element>
        {
            public const string Route = "Person";
            protected override string BaseRoute => Route;
        }
    }
}

﻿using System.Text.Json.Serialization;
using App.Domain;

namespace App.Web
{
    public class PostResponse<T>
        where T : IElement
    {
        public T Element
        {
            get;
            set;
        }
        public PostResponse(string BaseRoute)
        {
            this.BaseRoute = BaseRoute;
        }
        [JsonIgnore]
        private string BaseRoute
        {
            get;
            set;
        }
        public string Route => string.Format("{0}/{1}", BaseRoute, Element.ID);
    }
}

﻿

using App.Domain;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace App.Web
{
    namespace Core
    {
        [EnableCors(Startup.C.AllowAllOriginsPolicy)]
        [ApiController]
        public abstract class Controller<T, U> : ControllerBase, IController<U>
            where U :
                class, IElement
            where T :
                 IWorkflow<U>, new()
        {
            protected abstract string BaseRoute
            {
                get;
            }
            private T Workflow
            {
                get;
                set;
            }
            protected Controller()
            {
                Workflow = new T();
            }
            [HttpGet]
            [Route("{ID:int}")]
            public U Get(int ID) => Workflow.Get(ID);
            [HttpPost]
            [ProducesResponseType(StatusCodes.Status201Created)]
            public PostResponse<U> Post([FromBody] U Element)
            {
                Workflow.Add(Element);
                return new PostResponse<U>(BaseRoute)
                {
                    Element = Element
                };
            }
            [HttpPut]
            public void Put([FromBody] U Element) =>  Workflow.Update(Element);
            [HttpDelete]
            public void Delete([FromBody] U Element) => Workflow.Remove(Element);
        }
    }
}

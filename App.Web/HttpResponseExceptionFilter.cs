﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ValidationException = App.Domain.ValidationException;

namespace App.Web
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        public int Order { get; set; } = int.MaxValue - 10;
        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is ValidationException Exception)

                context.Result = new ObjectResult(Exception.Messages)
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            else if (context.Exception is System.Exception exception)
            {
                context.Result = new ObjectResult(exception.Message)
                {

                    StatusCode = StatusCodes.Status500InternalServerError
                };
            }
            context.ExceptionHandled = true;
        }
    }
}

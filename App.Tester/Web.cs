﻿using App.Data;
using App.Domain.Person;
using App.Web;
using App.Web.Person;
using Xunit;
using Xunit.Priority;
using Element = App.Domain.Person.Element;

namespace App.Tester
{
    public class Web
    {
        private (Element, IController<Element>) CreateEnviroment() =>
         (
             new Element()
             {
                 Address = new string('*', Validator.C.MIN_ADDRESS_LENGTH),
                 Age = Validator.C.MIN_AGE,
                 CountryOfOrigin = "aruba",
                 EMailAddress = "Test@Email.aw",
                 FamilyName = new string('*', Validator.C.MIN_FAMILYNAME_LENGTH),
                 Name = new string('*', Validator.C.MIN_NAME_LENGTH)
             },
             new Controller()
         );
        [Fact, Priority(-20)]
        public void Post()
        {
            (Element Element, IController<Element> Controller) = CreateEnviroment();
            Assert.Null(Element.ID);
            PostResponse<Element> Response = Controller.Post(Element);
            Assert.NotNull(Response.Element.ID);
        }
        [Fact, Priority(-10)]
        public void Get()
        {
            (Element Element, IController<Element> Controller) = CreateEnviroment();
            Controller.Post(Element);
            Element ElementFromCr = Controller.Get(Element.ID.Value);
            Assert.Equal(Element.ID, ElementFromCr.ID);
        }
        [Fact]
        public void Put()
        {
            (Element Element, IController<Element> Controller) = CreateEnviroment();
            Controller.Post(Element);
            Element.Address = new string('+', Validator.C.MIN_ADDRESS_LENGTH);
            Controller.Put(Element);
            Element ElementFromCr = Controller.Get(Element.ID.Value);
            Assert.Equal(Element.Address, ElementFromCr.Address);
        }
        [Fact]
        public void Delete()
        {
            (Element Element, IController<Element> Controller) = CreateEnviroment();
            Controller.Post(Element);
            Controller.Delete(Element);
            Assert.Throws<EntityNotFoundException>(() => Controller.Get(Element.ID.Value));
        }



    }
}

using App.Data;
using App.Data.Person;
using Xunit;
using Xunit.Priority;
using Entity = App.Data.Person.Entity;

namespace App.Tester
{
    [DefaultPriority(0)]
    public class Data
    {

        private (Entity, IDataBase<Entity>) CreateEnviroment() =>
                (
                    new Entity(),
                    new DataBase<Entity>()
                );

        [Fact, Priority(-30)]
        public void Entity()
        {
            (IEntity Entity, _) = CreateEnviroment();
            Assert.Null(Entity.ID);
        }
        [Fact, Priority(-20)]
        public void Add()
        {
            (Entity Entity, IDataBase<Entity> Db) = CreateEnviroment();
            Assert.Null(Entity.ID);
            Db.Add(Entity);
            Assert.NotNull(Entity.ID);
        }
        [Fact, Priority(-10)]
        public void Get()
        {
            (Entity Entity, IDataBase<Entity> Db) = CreateEnviroment();
            Db.Add(Entity);
            Entity EntityInDb = Db.Get(Entity.ID.Value);
            Assert.Equal(Entity.ID, EntityInDb.ID);
        }
        [Fact]
        public void Update()
        {
            (Entity Entity, IDataBase<Entity> Db) = CreateEnviroment();
            Db.Add(Entity);
            Entity EntityInDb = Db.Get(Entity.ID.Value);
            Assert.Equal(EntityInDb.Name, Entity.Name);
            Entity.Name = "*";
            Db.Update(Entity);
            EntityInDb = Db.Get(Entity.ID.Value);
            Assert.Equal(Entity.Name, EntityInDb.Name);
        }
        [Fact]
        public void Remove()
        {
            (Entity Entity, IDataBase<Entity> Db) = CreateEnviroment();
            Db.Add(Entity);
            Db.Remove(Entity);
            Assert.Throws<EntityNotFoundException>(() => Db.Get(Entity.ID.Value));
        }
        [Fact]
        public void Add_BadUsageException()
        {
            (Entity Entity, IDataBase<Entity> Db) = CreateEnviroment();
            Db.Add(Entity);
            Assert.Throws<BadUsageException>(() => Db.Add(Entity));
        }
        [Fact]
        public void Update_BadUsageException()
        {
            (Entity Entity, IDataBase<Entity> Db) = CreateEnviroment();
            Assert.Throws<BadUsageException>(() => Db.Update(Entity));
        }
    }
}

﻿using App.Data;
using App.Domain;
using App.Domain.Person;
using Xunit;
using Xunit.Priority;
using Element = App.Domain.Person.Element;

namespace App.Tester
{
    public class Domain
    {
        private (Element, IWorkflow<Element>) CreateEnviroment() =>
            (
                new Element()
                {
                    Address = new string('*', Validator.C.MIN_ADDRESS_LENGTH),
                    Age = Validator.C.MIN_AGE,
                    CountryOfOrigin = "aruba",
                    EMailAddress = "Test@Email.aw",
                    FamilyName = new string('*', Validator.C.MIN_FAMILYNAME_LENGTH),
                    Name = new string('*', Validator.C.MIN_NAME_LENGTH)
                },
                 new Workflow()
            );

        [Fact, Priority(-20)]
        public void Add()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Workflow.Add(Element);
            Assert.NotNull(Element.ID);
        }
        [Fact, Priority(-10)]
        public void Get()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Workflow.Add(Element);
            Element ElementFromWf = Workflow.Get(Element.ID.Value);
            Assert.Equal(Element.ID, ElementFromWf.ID);
        }
        [Fact]
        public void Update()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Workflow.Add(Element);
            Element.Address = new string('+', Validator.C.MIN_ADDRESS_LENGTH);
            Workflow.Update(Element);
            Element ElementFromWf = Workflow.Get(Element.ID.Value);
            Assert.Equal(Element.Address, ElementFromWf.Address);
        }
        [Fact]
        public void Remove()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Workflow.Add(Element);
            Workflow.Remove(Element);
            Assert.Throws<EntityNotFoundException>(() => Workflow.Get(Element.ID.Value));
        }
        [Fact]
        public void Validate_Name()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.Name = new string('*', Validator.C.MIN_NAME_LENGTH - 1);
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_FamilyName()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.FamilyName = new string('*', Validator.C.MIN_FAMILYNAME_LENGTH - 1);
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_Age()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.Age = Validator.C.MIN_AGE - 1;
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
            Element.Age = Validator.C.MAX_AGE + 1;
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_EmailTLD()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.EMailAddress = "Test@Email.UnkownTLD";
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_EmailRegExMissingLocalPart()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.EMailAddress = "@Email.aw";
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_EmailRegExMissingAt()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.EMailAddress = "Email.aw";
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_EmailRegExMissingDomainName()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.EMailAddress = "Email.aw";
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_EmailRegExMissingTDL()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.EMailAddress = "Test@Email.";
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
        [Fact]
        public void Validate_CountryOfOrigin()
        {
            (Element Element, IWorkflow<Element> Workflow) = CreateEnviroment();
            Element.CountryOfOrigin = "Wonderland";// unkown Origin
            Assert.Throws<ValidationException>(() => Workflow.Add(Element));
        }
    }
}
